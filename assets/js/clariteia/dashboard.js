"use strict";
var ui_hostname;
var api_hostname;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
  ui_hostname = "http://localhost:8080";
  api_hostname = "http://localhost:3001";
} else {
  ui_hostname = "https://pmo-ui.bitbucket.io";
  api_hostname = "https://pmo-api.herokuapp.com";
}

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

// Class definition
function generateBubbleData(baseval, count, yrange) {
    var i = 0;
    var series = [];
    while (i < count) {
      var x = Math.floor(Math.random() * (750 - 1 + 1)) + 1;;
      var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
      var z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;
  
      series.push([x, y, z]);
      baseval += 86400000;
      i++;
    }
    return series;
  }

function generateData(count, yrange) {
    var i = 0;
    var series = [];
    while (i < count) {
        var x = 'w' + (i + 1).toString();
        var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

        series.push({
            x: x,
            y: y
        });
        i++;
    }
    return series;
}

var KTApexChartsDemo = function () {
	// Private functions
	

	var _demo2 = function () {
		const apexChart = "#chart_2";
		var options = {
			series: [{
				name: 'Validaciones',
				data: [723,476,1157,722,411,1157,896,1079,425,1128,915,831]
			}, {
				name: 'Denegaciones',
				data: [337,428,174,241,432,174,299,135,446,141,314,214]
			}, {
				name: 'Fraude',
				data: [145,286,115,240,185,115,298,135,191,141,78,142]
			}, {
				name: 'Declarantes proponentes',
				data: [1205,1190,1446,1203,1028,1446,1493,1349,1062,1410,1307,1187]
			}],
			chart: {
				height: 350,
				type: 'area'
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				curve: 'smooth'
			},
			xaxis: {
				categories: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
			},
			colors: [primary, warning, danger, success]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}

	var _demo3 = function () {
		const apexChart = "#chart_3";
		var options = {
			series: [{
				name: 'Número de validaciones',
				data: [14, 20, 10, 12, 8, 15, 20, 6, 12, 14, 19, 20]
			}, {
				name: 'Número de denegaciones',
				data: [4, 5, 3, 9, 7, 3, 5, 6, 3, 7, 3, 5]
			}, {
				name: 'Número de fraude',
				data: [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4]
			}],
			chart: {
				type: 'bar',
				height: 350
			},
			plotOptions: {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
			},
			fill: {
				opacity: 1
			},
			colors: [primary, warning, danger]
		};

		var chart = new ApexCharts(document.querySelector(apexChart), options);
		chart.render();
	}
  
	return {
		// public functions
		init: function () {
			_demo2();
			_demo3();
		}
	};
}();

jQuery(document).ready(function () {
	KTApexChartsDemo.init();
});


// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

"use strict";
// Class definition

var KTDatatableJsonRemoteDemo = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/urgent_declarations.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/

            // columns definition
            columns: [{
              field: 'actuacion',
              title: 'Actuación',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" class="">' + row.actuacion + '</a>';
              },
            }, {
                field: 'soporte',
                title: 'Soporte',
                template: function(row) {
                  return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" class="">' + row.soporte + '</a>';
                },
            /*}, {
                field: 'tocadas',
                title: 'Número de tocadas',
                width: 100,
              textAlign: 'center',*/
            }, {
                field: 'dec_ptes',
                title: 'Declaraciones pendientes',
                width: 150,
              textAlign: 'center',
            }, {
                field: 'dias',
                title: 'Días para la declaración más urgente',
                width: 200,
              textAlign: 'center',
              template: function(row) {
                if (row.dias <= 3) {
                  return '<span class="text-danger font-weight-bold">' + row.dias + '</span>'
                } else {
                  return row.dias
                }
              },
            }
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


jQuery(document).ready(function() {
  $( "#toggle_xor_filters" ).click(function(event) {
    event.preventDefault();
    $("#xor_search").toggleClass('d-none');
  });
  
  KTFormRepeater.init();
  //KTDatatablesSearchOptionsAdvancedSearch.init();
  KTDatatableJsonRemoteDemo.init();
});

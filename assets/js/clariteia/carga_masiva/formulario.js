"use strict";
// Class definition
var KTSelect2 = function() {
    // Private functions
    var demos = function() {
      
      $('#kt_select1').select2({
          placeholder: "David Bisball",
          allowClear: true
      });
      
      $('#kt_select_row2_nom_socio').select2({
          placeholder: "Pablo A",
          allowClear: true
      });
      
      $('#kt_select_row11_referencia').select2({
          placeholder: "43f",
          allowClear: true
      });
      
      $('#kt_select_row_3_ap').select2({
          placeholder: "Fito y",
          allowClear: true
      });
      
      $('#kt_select_row_3_soporte').select2({
          placeholder: "Bon Ivere",
          allowClear: true
      });
      
      $('#kt_select_row_3_actuacion').select2({
          placeholder: "Appetite for Destruction",
          allowClear: true
      });
      
      
      $('#kt_select_row3_referencia').select2({
          placeholder: "3445t3",
          allowClear: true
      });
      
      $('#kt_select_row11_nom_socio').select2({
          placeholder: "Juan P",
          allowClear: true
      });
      
      $('#kt_select_row11_num_socio').select2({
          placeholder: "Rosalía Ew",
          allowClear: true
      });
      
      $('#kt_select_row11_modalidad').select2({
          placeholder: "Interprte",
          allowClear: true
      });
      
      $('#kt_select_row11_instrumento').select2({
          placeholder: "Gtara",
          allowClear: true
      });
      
      $('#kt_select_row3_modalidad').select2({
          placeholder: "DMF",
          allowClear: true
      });
      
      $('#kt_select_row3_instrumento').select2({
          placeholder: "Instrumento",
          allowClear: true
      });
      
      $('#kt_select_row2_num_socio').select2({
          placeholder: "8734874jj34",
          allowClear: true
      });
      
      $('#kt_select2').select2({
          placeholder: "No Jacket Requireded",
          allowClear: true
      });
      
      $('#kt_select3').select2({
          placeholder: "Cross Roadf",
          allowClear: true
      });
    }

    // Public functions
    return {
        init: function() {
            demos();
        }
    };
}();


var KTDatatablesDataSourceHtml = function() {

	var initTable1 = function() {
		var table = $('#kt_datatable');

		// begin first table
		table.DataTable({
			responsive: true,
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
  KTSelect2.init();
});

"use strict";

var ui_hostname;
var api_hostname;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
  ui_hostname = "http://localhost:8080";
  api_hostname = "http://localhost:3001";
} else {
  ui_hostname = "https://pmo-ui.bitbucket.io";
  api_hostname = "https://pmo-api.herokuapp.com";
}

function load_detail(event) {
  event.preventDefault();
  $('.nav-tabs a[href="#kt_tab_pane_2_4"]').tab('show');
}

function button_pressed(event) {
  $(event).css("color", "#ffffff !important");
  $(event).css("background-color", "#161617 !important");
  $(event).css("border-color", "#494F57 !important");
}

// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

// Class definition

var KTDatatableJsonRemoteDemo = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/bulk_load.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/

            // columns definition
            columns: [{
              field: 'artist',
              title: 'Socio',
              template: function(row) {
                return '<a href="carga_masiva/carga_masiva_detalle.html" >' + row.artist + '</a>';
              },
            }, {
              field: 'q_declarations',
              title: 'Numero de declaraciones',
              template: function(row) {
                return '<a href="carga_masiva/carga_masiva_detalle.html" >' + row.q_declarations + '</a>';
              },
            }, {
                field: 'reg_date',
                title: 'Fecha',
                //width: 70,
                template: function(row) {
                  return '<a href="carga_masiva/carga_masiva_detalle.html" ">' + row.reg_date + '</a>';
                },
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();

var KTDatatableBulkLoadDetail = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable_2').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/bulk_load_detail.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/

            // columns definition
            columns: [{
              field: 'actuacion',
              title: 'Canciones',
              //textAlign: 'center',
              width: 300,
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" >' + row.actuacion + '</a>';
              },
            }, {
              field: 'instrument',
              title: 'Instrumento',
              //textAlign: 'center',
              width: 100,
            }, {
              field: 'modality',
              title: 'Modalidad',
              //textAlign: 'center',
              width: 90,
            }, {
              field: 'actions',
              title: 'Acciónes',
              //textAlign: 'center',
              //width: 200,
              template: function(row) {
                return '<a class="btn btn-sm btn-light-success font-weight-bolder text-uppercase mr-3 button_press">Aprobar</a>\
												<a href="#" class="btn btn-sm btn-primary font-weight-bolder text-uppercase mr-3" data-toggle="modal" data-target="#devolverDeclaracion" >Devolver</a>\
                        <a href="#" class="btn btn-sm btn-warning font-weight-bolder text-uppercase mr-3" data-toggle="modal" data-target="#exampleModalLong" >Rechazar</a>\
                        <a class="btn btn-sm btn-danger font-weight-bolder text-uppercase" data-toggle="modal" data-target="#FraudeModalPopUp">Fraude</a>';
              },
            },
          ],

        });
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


jQuery(document).ready(function() {
  
  $( "#toggle_xor_filters" ).click(function(event) {
    event.preventDefault();
    $("#xor_search").toggleClass('d-none');
  });
  
  KTFormRepeater.init();
  KTDatatableJsonRemoteDemo.init();
  KTDatatableBulkLoadDetail.init();
  
  $( ".button_press" ).on('click', function(event){
    console.log("TEST");
    $(event).css("color", "#ffffff !important");
    $(event).css("background-color", "#161617 !important");
    $(event).css("border-color", "#494F57 !important");
  });
});

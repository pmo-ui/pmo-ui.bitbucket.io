// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

"use strict";
// Class definition

var KTDatatableJsonRemoteDemo = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: "https://pmo-api.herokuapp.com/api/v1/track_history.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/

            // columns definition
            columns: [{
              field: 'proponent',
              title: 'Proponente',
            }, {
              field: 'concept',
              title: 'Concepto',
            }, {
              field: 'reg_date',
              title: 'Fecha',
              width: 120,
            }, {
              field: 'btn_1',
              title: 'Certificado',
              width: 180,
              template: function(row) {
                return '<a href="#" class="btn btn-sm btn-light-success font-weight-bolder text-uppercase mr-3">Mostrar certificado</a>';
              },
            }, {
              field: 'btn_2',
              title: 'Gestionar error',
              width: 150,
              template: function(row) {
                return '<button type="button" class="btn btn-sm btn-warning font-weight-bolder text-uppercase mr-3" data-toggle="modal" data-target="#exampleModalLong">Gestionar error</button>';
              },
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


jQuery(document).ready(function() {
  $( "#toggle_xor_filters" ).click(function(event) {
    event.preventDefault();
    $("#xor_search").toggleClass('d-none');
  });
  
  KTFormRepeater.init();
  //KTDatatablesSearchOptionsAdvancedSearch.init();
  KTDatatableJsonRemoteDemo.init();
  
  $( "#lock_unlock_karma" ).hover(
    function() {
      $( this ).removeClass( "fa-lock-open text-danger" ).addClass( "fa-lock text-success" );
      $( "#karma_status" ).text("Bloquear");
    }, function() {
      $( "#karma_status" ).text("Desbloqueado");
      $( "#lock_unlock_karma" ).removeClass( "fa-locks text-success" ).addClass( "fa-lock-open text-danger" );
    }
  );
});

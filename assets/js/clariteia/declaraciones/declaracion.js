"use strict";

var ui_hostname;
var api_hostname;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
  ui_hostname = "http://localhost:8080";
  api_hostname = "http://localhost:3001";
} else {
  ui_hostname = "https://pmo-ui.bitbucket.io";
  api_hostname = "https://pmo-api.herokuapp.com";
}

// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();


// Class definition
var KTDatatableProposingDeclarants = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable_declarants').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/proposing_declarants.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/
              // {instrument: 'Bajo', date: '22/11/2020', limit_date: '22/12/2020', like_dislike: '', evidence: true},
            // columns definition
            columns: [{
              field: 'karma',
              title: 'Karma',
              textAlign: 'center',
              width: 50,
              template: function(row) {
                var star;
                var value;
                var color;
                var star_fill;
                
                /*if(row.karma <= 4) {
                  color = "text-danger";
                } else {
                  if(row.karma > 4 && row.karma <= 7) {
                    color = "text-warning";
                  } else {
                    if(row.karma > 7) {
                      color = "text-success";
                    }
                  }
                }
                
                star = '<i class="fas fa-star icon-md ' + color + '" style="float:right;" data-container="body" data-toggle="tooltip" data-placement="top" title="Karma: '+ row.karma +' (de 10)"></i>';
                */
                
                  if (row.karma <= 4) {
                    color = "text-danger";
                    star_fill = "far fa-star";
                  } else {
                    if(row.karma > 4 && row.karma <= 7) {
                      color = "text-warning";
                      star_fill = "fa fa-star-half-alt";
                    } else {
                      if(row.karma > 7) {
                        color = "text-success";
                        star_fill = "fa fa-star";
                      }
                    }
                  }
                
                star = '<i class="' + star_fill + ' mr-1 ' + color + ' font-size-lg"></i>\
                <span class="text-dark-75 font-weight-bolder">' + row.karma + '</span>';  
                  
                return star;
              },
            }, {
              field: 'artist',
              title: 'Artista',
              textAlign: 'center',
              width: 200,
              template: function(row) {
    						var number = KTUtil.getRandomInt(1, 14);
    						var user_img = 'background-image:url(\'assets/media/users/100_' + number + '.jpg\')';

    						var output = '';
    						if (number > 8) {
    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 flex-shrink-0">\
    									<div class="symbol-label" style="' + user_img + '"></div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.artist + '</div>\
    								</div>\
    							</div>';
    						}
    						else {
    							var stateNo = KTUtil.getRandomInt(0, 7);
    							var states = [
    								'success',
    								'primary',
    								'danger',
    								'success',
    								'warning',
    								'dark',
    								'primary',
    								'info'];
    							var state = states[stateNo];

    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 symbol-'+state+' flex-shrink-0">\
    									<div class="symbol-label">' + row.artist.substring(0, 1) + '</div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.artist + '</div>\
    								</div>\
    							</div>';
    						}

    						return output;
    					},
            }, {
              field: 'instrument',
              title: 'Instrumento',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/historial.html" class="">' + row.instrument + '</a>';
              },
            }, {
                field: 'modality',
                title: 'Modalidad',
                textAlign: 'center',
                width: 120,
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();

var KTDatatableDeclarants = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable_proponents_declarants').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/declarants.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/
              // {instrument: 'Bajo', date: '22/11/2020', limit_date: '22/12/2020', like_dislike: '', evidence: true},
            // columns definition
            columns: [{
              field: 'karma',
              title: 'Karma',
              textAlign: 'center',
              width: 50,
              template: function(row) {
                var star;
                var value;
                var color;
                var star_fill;
                
                /*if(row.karma <= 4) {
                  color = "text-danger";
                } else {
                  if(row.karma > 4 && row.karma <= 7) {
                    color = "text-warning";
                  } else {
                    if(row.karma > 7) {
                      color = "text-success";
                    }
                  }
                }
                
                star = '<i class="fas fa-star icon-md ' + color + '" style="float:right;" data-container="body" data-toggle="tooltip" data-placement="top" title="Karma: '+ row.karma +' (de 10)"></i>';
                */
                
                  if (row.karma <= 4) {
                    color = "text-danger";
                    star_fill = "far fa-star";
                  } else {
                    if(row.karma > 4 && row.karma <= 7) {
                      color = "text-warning";
                      star_fill = "fa fa-star-half-alt";
                    } else {
                      if(row.karma > 7) {
                        color = "text-success";
                        star_fill = "fa fa-star";
                      }
                    }
                  }
                
                star = '<i class="' + star_fill + ' mr-1 ' + color + ' font-size-lg"></i>\
                <span class="text-dark-75 font-weight-bolder">' + row.karma + '</span>';  
                  
                return star;
              },
            }, {
              field: 'karma_locked',
              title: 'Afectación al karma',
              textAlign: 'center',
              width: 120,
              template: function(row) {
                console.log(row.karma_locked);
                if (row.karma_locked) {
                  return '<i class=" mr-1 fas fa-lock text-success icon-lg"></i>'
                } else {
                  return '<i class=" mr-1 fas fa-lock-open text-danger icon-lg"></i>'
                }
              },
            }, {
              field: 'artist',
              title: 'Artista',
              textAlign: 'center',
              width: 200,
              template: function(row) {
    						var number = KTUtil.getRandomInt(1, 14);
    						var user_img = 'background-image:url(\'assets/media/users/100_' + number + '.jpg\')';

    						var output = '';
    						if (number > 8) {
    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 flex-shrink-0">\
    									<div class="symbol-label" style="' + user_img + '"></div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.artist + '</div>\
    								</div>\
    							</div>';
    						}
    						else {
    							var stateNo = KTUtil.getRandomInt(0, 7);
    							var states = [
    								'success',
    								'primary',
    								'danger',
    								'success',
    								'warning',
    								'dark',
    								'primary',
    								'info'];
    							var state = states[stateNo];

    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 symbol-'+state+' flex-shrink-0">\
    									<div class="symbol-label">' + row.artist.substring(0, 1) + '</div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.artist + '</div>\
    								</div>\
    							</div>';
    						}

    						return output;
    					},
            }, {
              field: 'instrument',
              title: 'Instrumento',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/detalle_declaracion.html" class="">' + row.instrument + '</a>';
              },
            }, {
                field: 'modality',
                title: 'Modalidad',
                textAlign: 'center',
                width: 120,
            }, {
                field: 'date',
                title: 'Fecha',
                textAlign: 'center',
                width: 80,
            }, {
                field: 'limit_date',
                title: 'Fecha limite',
                textAlign: 'center',
                width: 100,
            }, {
              field: 'like',
              title: 'Like/Dislike',
              textAlign: 'center',
              width: 100,
              template: function(row) {
                var like = '<i class="far fa-thumbs-up mr-1 text-primary icon-lg"></i>\
                <span class="text-dark-75 font-weight-bolder">' + row.likes + '</span>';
                
                var dislike = '<i class="far fa-thumbs-down mr-1 text-danger icon-lg"></i>\
                <span class="text-dark-75 font-weight-bolder">' + row.dislikes + '</span>';
                
                return like + "<br>" +dislike;
              },
            }, {
                field: 'evidence',
                title: 'Evidencias',
                textAlign: 'center',
                width: 90,
              template: function(row) {
                if (row.evidence) {
                  return '<label class="checkbox checkbox-disabled ml-5"><input type="checkbox" disabled="disabled" checked="checked" name="Checkboxes1" /><span></span></label>'
                } else {
                  return '<label class="checkbox checkbox-disabled ml-5"><input type="checkbox" disabled="disabled" name="Checkboxes1" /><span></span></label>'
                }
              }
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();

var KTDatatableRelatedDeclarants = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable_related_declarants').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/related_declarants.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/
              // {instrument: 'Bajo', date: '22/11/2020', limit_date: '22/12/2020', like_dislike: '', evidence: true},
            // columns definition
            columns: [{
              field: 'instrument',
              title: 'Instrumento',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/detalle_declaracion.html" class="">' + row.instrument + '</a>';
              },
            }, {
              field: 'to',
              title: 'Para',
              template: function(row) {
    						var number = KTUtil.getRandomInt(1, 14);
    						var user_img = 'background-image:url(\'assets/media/users/100_' + number + '.jpg\')';

    						var output = '';
    						if (number > 8) {
    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 flex-shrink-0">\
    									<div class="symbol-label" style="' + user_img + '"></div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.to + '</div>\
    								</div>\
    							</div>';
    						}
    						else {
    							var stateNo = KTUtil.getRandomInt(0, 7);
    							var states = [
    								'success',
    								'primary',
    								'danger',
    								'success',
    								'warning',
    								'dark',
    								'primary',
    								'info'];
    							var state = states[stateNo];

    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 symbol-'+state+' flex-shrink-0">\
    									<div class="symbol-label">' + row.to.substring(0, 1) + '</div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.to + '</div>\
    								</div>\
    							</div>';
    						}

    						return output;
    					},
            }, {
              field: 'from',
              title: 'De',
              template: function(row) {
								var template ='<div class="symbol-group symbol-hover">\
													<div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Mark Stone">\
														<img alt="Pic" src="assets/media/users/300_25.jpg" />\
													</div>\
													<div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Charlie Stone">\
														<img alt="Pic" src="assets/media/users/300_19.jpg" />\
													</div>\
													<div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Luca Doncic">\
														<img alt="Pic" src="assets/media/users/300_22.jpg" />\
													</div>\
													<div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Nick Mana">\
														<img alt="Pic" src="assets/media/users/300_23.jpg" />\
													</div>\
													<div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="Teresa Fox">\
														<img alt="Pic" src="assets/media/users/300_18.jpg" />\
													</div>\
													<div class="symbol symbol-30 symbol-circle symbol-light">\
														<span class="symbol-label font-weight-bold">5+</span>\
													</div>\
												</div>'
                
                
                return template;
              },
            }, /*{
                field: 'date',
                title: 'Fecha',
                width: 80,
            }, {
                field: 'limit_date',
                title: 'Fecha limite',
                width: 100,
            }, {
                field: 'like',
                title: 'Like/Dislike',
                width: 100,
                template: function(row) {
                  var like = '<i class="far fa-thumbs-up mr-1 text-primary icon-lg"></i>\
                  <span class="text-dark-75 font-weight-bolder">' + row.likes + '</span>';
                  
                  var dislike = '<i class="far fa-thumbs-down mr-1 text-danger icon-lg"></i>\
                  <span class="text-dark-75 font-weight-bolder">' + row.dislikes + '</span>';
                  
                  return like + "<br>" +dislike;
                },
            }, {
                field: 'evidence',
                title: 'Evidencias',
                textAlign: 'center',
                width: 90,
              template: function(row) {
                if(row.evidence) {
                  return '<label class="checkbox checkbox-disabled ml-5"><input type="checkbox" disabled="disabled" checked="checked" name="Checkboxes1" /><span></span></label>'
                } else {
                  return '<label class="checkbox checkbox-disabled ml-5"><input type="checkbox" disabled="disabled" name="Checkboxes1" /><span></span></label>'
                }
              }
            },*/
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


var KTDatatableSuggestedDeclarants = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable_suggested_declarants').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/suggested_declarants.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/
              // {instrument: 'Bajo', date: '22/11/2020', limit_date: '22/12/2020', like_dislike: '', evidence: true},
            // columns definition
            columns: [{
              field: 'instrument',
              title: 'Instrumento',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" class="">' + row.instrument + '</a>';
              },
            }, {
              field: 'artist',
              title: 'Artista',
              textAlign: 'center',
              width: 400,
              template: function(row) {
    						var number = KTUtil.getRandomInt(1, 14);
    						var user_img = 'background-image:url(\'assets/media/users/100_' + number + '.jpg\')';

    						var output = '';
    						if (number > 8) {
    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 flex-shrink-0">\
    									<div class="symbol-label" style="' + user_img + '"></div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.artist + '</div>\
    								</div>\
    							</div>';
    						}
    						else {
    							var stateNo = KTUtil.getRandomInt(0, 7);
    							var states = [
    								'success',
    								'primary',
    								'danger',
    								'success',
    								'warning',
    								'dark',
    								'primary',
    								'info'];
    							var state = states[stateNo];

    							output = '<div class="d-flex align-items-center">\
    								<div class="symbol symbol-40 symbol-'+state+' flex-shrink-0">\
    									<div class="symbol-label">' + row.artist.substring(0, 1) + '</div>\
    								</div>\
    								<div class="ml-2">\
    									<div class="text-dark-75 font-weight-bold line-height-sm">' + row.artist + '</div>\
    								</div>\
    							</div>';
    						}

    						return output;
    					},
            }, {
                field: 'modality',
                title: 'Modalidad',
                width: 120,
            }, {
                field: 'coincidence',
                title: 'Coincidencia',
                width: 120,
            }, {
                field: 'reason',
                title: 'Razón',
                width: 300,
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormRepeater.init();
    KTDatatableProposingDeclarants.init();
    KTDatatableDeclarants.init();
    KTDatatableRelatedDeclarants.init();
    KTDatatableSuggestedDeclarants.init();
});

"use strict";

var ui_hostname;
var api_hostname;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
  ui_hostname = "http://localhost:8080";
  api_hostname = "http://localhost:3001";
} else {
  ui_hostname = "https://pmo-ui.bitbucket.io";
  api_hostname = "https://pmo-api.herokuapp.com";
}

// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

// Class definition

var KTDatatableJsonRemoteDemo = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/declarations.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/

            // columns definition
            columns: [{
              field: 'main_artist',
              title: 'Artista principal',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" class="">' + row.main_artist + '</a>';
              },
            }, {
              field: 'album',
              title: 'Album',
              template: function(row) {
                return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" class="">' + row.album + '</a>';
              },
            }, {
                field: 'title',
                title: 'Tema',
                width: 70,
                template: function(row) {
                  return '<a href="' + ui_hostname + '/declaraciones/declaracion.html" class="">' + row.title + '</a>';
                },
            }, {
                field: 'year',
                title: 'Año',
                width: 40,
            }, {
                field: 'style',
                title: 'Estilo',
                width: 50,
            }, {
                field: 'genre',
                title: 'Genero',
                width: 60,
            }, {
                field: 'declarant_participant',
                title: 'Declarantes / participantes',
                textAlign: 'center',
            }, {
                field: 'pending_declarants',
                title: 'Declarantes pendientes',
                textAlign: 'center',
                template: function(row) {
                  if (row.pending_declarants == 0) {
                    return '<i class="fas fa-check mr-1 text-success icon-lg"></i>'
                  } else {
                    return row.pending_declarants
                  }
                },
            }, {
                field: 'interpreters',
                title: 'Interpretes',
                textAlign: 'center',
                width: 60,
            }, {
                field: 'performers',
                title: 'Ejecutantes',
                textAlign: 'center',
                width: 60,
            }, {
                field: 'dmg',
                title: 'DMG',
                width: 40,
                textAlign: 'center',
                template: function(row) {
                  if (row.dmg == 0) {
                    return '<i class="flaticon2-cancel mr-1 text-danger icon-lg"></i>'
                  } else {
                    return row.dmg
                  }
                },
            }, {
                field: 'related_declarants',
                title: 'Declarantes relacionados',
                textAlign: 'center',
            }, {
                field: 'played',
                title: 'Tocadas',
                textAlign: 'center',
                width: 70,
            }, {
                field: 'remaining_days',
                title: 'Días ptes.',
                textAlign: 'center',
                template: function(row) {
                  if (row.remaining_days <= 3) {
                    return '<span class="text-danger font-weight-bold">' + row.remaining_days + '</span>'
                  } else {
                    return row.remaining_days
                  }
                },
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


jQuery(document).ready(function() {
  $( "#toggle_xor_filters" ).click(function(event) {
    event.preventDefault();
    $("#xor_search").toggleClass('d-none');
  });
  
  KTFormRepeater.init();
  //KTDatatablesSearchOptionsAdvancedSearch.init();
  KTDatatableJsonRemoteDemo.init();
});

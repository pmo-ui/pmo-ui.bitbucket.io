"use strict";

var ui_hostname;
var api_hostname;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
  ui_hostname = "http://localhost:8080";
  api_hostname = "http://localhost:3001";
} else {
  ui_hostname = "https://pmo-ui.bitbucket.io";
  api_hostname = "https://pmo-api.herokuapp.com";
}

// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();


// Class definition

var KTDatatableDeclarants = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable_declarants').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/declaration_detail.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },
              // {instrument: 'Bajo', date: '22/11/2020', limit_date: '22/12/2020', like_dislike: '', evidence: true},
            // columns definition
            columns: [{
              field: 'declarant',
              title: 'Declarante',
              /*template: function(row) {
                return '<a href="https://pmo-ui.bitbucket.io/declaraciones/declaracion.html" class="">' + row.instrument + '</a>';
              },*/
            }, {
                field: 'colaborations',
                title: 'Nº de canciones en las que colaboran',
                width: 220,
            }, {
                field: 'percent_1',
                title: '% sobre el declarante',
                width: 120,
            }, {
                field: 'percent_2',
                title: '% sobre el declarante proponente',
                width: 80,
            },
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


var KTDropzoneDemo = function () {
    // Private functions
    var demo1 = function () {
        // single file upload
        // file type validation
        $('#kt_dropzone_3').dropzone({
            url: "https://keenthemes.com/scripts/void.php", // Set the url for your upload script location
            paramName: "file", // The name that will be used to transfer the file
            maxFiles: 10,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: "image/*,application/pdf,.psd",
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

KTUtil.ready(function() {
    KTDropzoneDemo.init();
});

jQuery(document).ready(function() {
    KTFormRepeater.init();
    KTDatatableDeclarants.init();
    
    
});

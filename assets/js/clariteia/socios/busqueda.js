"use strict";

var ui_hostname;
var api_hostname;

if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
  ui_hostname = "http://localhost:8080";
  api_hostname = "http://localhost:3001";
} else {
  ui_hostname = "https://pmo-ui.bitbucket.io";
  api_hostname = "https://pmo-api.herokuapp.com";
}

// Class definition
var KTFormRepeater = function() {

    // Private functions
    var demo1 = function() {
        $('#kt_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

var KTDatatableJsonRemoteDemo = function() {
    // Private functions

    // basic demo
    var demo = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: api_hostname + "/api/v1/aie_members.json", //HOST_URL + '/api/?file=datatables/datasource/default.json',
                pageSize: 10,
                method:'GET',
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            /*search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },*/

            // columns definition
            columns: [{
              field: 'member_name',
              title: 'Nombre de socio',
              template: function(row) {
                return '<a href="' + ui_hostname + '/socios/perfil.html" class="">' + row.member_name + '</a>';
              },
            }, {
              field: 'member_id',
              title: 'Número de socio',
              template: function(row) {
                return '<a href="' + ui_hostname + '/socios/perfil.html" class="">' + row.member_id + '</a>';
              },
            }, {
                field: 'main_artist',
                title: 'Artista principal',
                template: function(row) {
                  return '<a href="' + ui_hostname + '/socios/perfil.html" class="">' + row.main_artist + '</a>';
                },
            }, {
                field: 'pending_declarants',
                title: 'Declarantes pendientes',
                textAlign: 'center',
                template: function(row) {
                  if (row.pending_declarants == 0) {
                    return '<i class="fas fa-check mr-1 text-success icon-lg"></i>'
                  } else {
                    return row.pending_declarants
                  }
                },
            }, {
                field: 'related_declarants',
                title: 'Declarantes relacionados',
                textAlign: 'center',
            }, {
                field: 'interpreters',
                title: 'Interpretes',
                textAlign: 'center',
            }, {
                field: 'performers',
                title: 'Ejecutantes',
                textAlign: 'center',
                width: 120,
            }, {
                field: 'dmg',
                title: 'DMG',
                textAlign: 'center',
                width: 80,
                template: function(row) {
                  if (row.dmg == 0) {
                    return '<i class="flaticon2-cancel mr-1 text-danger icon-lg"></i>'
                  } else {
                    return row.dmg
                  }
                },
            }
          ],

        });

       /* $('#kt_datatable_search_status').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#kt_datatable_search_type').on('change', function() {
            datatable.search($(this).val().toLowerCase(), 'Type');
        });

        $('#kt_datatable_search_status, #kt_datatable_search_type').selectpicker();*/
    };

    return {
        // public functions
        init: function() {
            demo();
        }
    };
}();


jQuery(document).ready(function() {
  $( "#toggle_xor_filters" ).click(function(event) {
    event.preventDefault();
    $("#xor_search").toggleClass('d-none');
  });
  
  KTFormRepeater.init();
  //KTDatatablesSearchOptionsAdvancedSearch.init();
  KTDatatableJsonRemoteDemo.init();
});

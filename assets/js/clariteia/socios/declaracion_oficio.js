"use strict";
// Class definition
var KTSelect2 = function() {
    // Private functions
    var demos = function() {
      
      $('#kt_select').select2({
        placeholder: "Búsqueda por socio",
        allowClear: true
      });
      
      $('#kt_select1').select2({
        placeholder: "Búsqueda por modalidad",
        allowClear: true
      });
      
      $('#kt_select11').select2({
        placeholder: "Búsqueda por soporte",
        allowClear: true
      });
      
      $('#kt_select2').select2({
        placeholder: "Búsqueda por instrumento",
        allowClear: true
      });
    }

    // Public functions
    return {
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
  KTSelect2.init();
});
